public class Student{
	private String Nim;
	private String nama;
	private String tanggallahir;
	
	public void setNim(String _nim){
		this.Nim = _nim;
	}
	public void setNama(String _nama){
		this.nama = _nama;
	}
	public void setTanggalLahir(String _tglLahir){
		this.tanggallahir = _tglLahir;
	}
	
	public String getNim(){
		return this.Nim;
	}
	public String getNama(){
		return this.nama;
	}
	public String getTanggallahir(){
		return this.tanggallahir;
	}
}
